package INF101.lab2;

import java.security.InvalidAlgorithmParameterException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    
    // FridgeItem[] fridgeArray = new FridgeItem[20];
    private ArrayList<FridgeItem> fridgeArray;
    private int maxSize;

    public Fridge(){
        fridgeArray = new ArrayList<>();
        maxSize = 20;
    }

    
    @Override
    public int nItemsInFridge() { 
        int itemCount = 0;
        for (int i = 0; i < fridgeArray.size(); i++) {
            if (fridgeArray.get(i) != null) {
                itemCount++;
            }
        }
        return itemCount;
    }

    @Override
    public int totalSize() {
        
        return this.maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        
        if (nItemsInFridge() == 20) {
            return false;
        } 
        else {
            fridgeArray.add(item);
        }
        return true;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (nItemsInFridge() == 0) {
            throw new NoSuchElementException();
        }
        else {
            fridgeArray.remove(item);
        }
            
        
    }

    @Override
    public void emptyFridge() {
        fridgeArray.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
        
        for (FridgeItem foodItem : fridgeArray) {
            if (foodItem.hasExpired()) {
                expiredItems.add(foodItem);
            }
        }
        fridgeArray.removeAll(expiredItems);
        return expiredItems;
        
}
}
